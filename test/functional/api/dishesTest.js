const chai = require("chai");
const server = require("../../../bin/www");
const expect = chai.expect;
const request = require("supertest");
const _ = require("lodash");

let datastore = require("../../../models/dishes");

describe('Dishes', () => {
    beforeEach(() => {
        while (datastore.length > 0){
            datastore.pop();
        }
        datastore.push({
            _id: "5df1660d81b33555e039a8c8",
            name: "hamburger",
            code: "01",
            price : 12,
            upvotes : 0
        });

        datastore.push({
            _id: "5df1660d81b33555e039a8c9",
            name: "meat",
            code: "02",
            price : 1200,
            upvotes : 0
        });
    })

    describe("GET /dishes", () => {
        it("should return all the dishes", done => {
            request(server)
                .get("/dishes")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body).to.be.a("array");
                    expect(res.body.length).to.equal(7);
                    datastore = res.body;
                    let result = _.map(res.body, dished => {
                        return {
                            name: dished.name,
                            code: dished.code,
                            price: dished.price
                        };
                    });
                    expect(result).to.deep.include({
                        name: "hamburger",
                        code: "01",
                        price: 12
                    });
                    expect(result).to.deep.include({
                        name: "meat",
                        code: "02",
                        price: 1200
                    });
                    done(err);
                });
        });
    });

    describe("GET /dishes/:id", () => {
        describe("when the id is valid", () => {
            it("should return the mathcing dish", done => {
                request(server)
                    .get(`/dishes/${datastore[0]._id}`)
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body).to.deep.include(datastore[0]);
                        done(err);
                    });
            });
        });
        describe("when thhe id is invalid", () => {
            it("should return the NOT found message", done => {
                request(server)
                    .get('/dishes/9999')
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .expect({message: "Dishes NOT Found!"}, (err, res) => {
                        done(err);
                    });
            })
        })
    })

    describe("POST /dishes", () => {
        it("should return confirmation message and update datastore", () => {
            const dishes = {
                name: "noodles",
                code: "03",
                price: 15
            }
            return request(server)
                .post("/dishes")
                .send(dishes)
                .expect(200)
                .expect({message: "Dishes Successfully Added!"});
        });
        after(() => {
            return request(server)
                .get('/dishes')
                .expect(200)
                .then(res => {
                    expect(res.body.length).equals(8);
                    datastore = res.body;
                    const result = _.map(res.body, dished => {
                        return {
                            name: dished.name,
                            code: dished.code,
                            price: dished.price
                        };
                    });
                    expect(result).to.deep.include({
                        name: "noodles",
                        code: "03",
                        price: 15
                    });
                })
        })
    })

    describe("PUT /dishes/:id/vote", () => {
        describe("when the id is valid", () => {
            it("should return a message and the dishes upvoted by 1", done => {
                request(server)
                    .put(`/dishes/${datastore[1]._id}/vote`)
                    .expect(200)
                    .then(resp => {
                        expect(resp.body).to.include({message: "Dishes Successfully Upvoted!"});
                        expect(resp.body.data).to.include({
                            _id: datastore[1]._id,
                            upvotes: 1
                        });
                    });
            });
            after(() => {
                request(server)
                    .get(`/dishes/${datastore[1]._id}`)
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .then(res => {
                        expect(res.body).to.deep.include({_id: datastore[1]._id, upvotes: 1});
                    });
            });
        });
        describe("when the id is invalid", () => {
            it("should return a 404 and a message for invalid donation id", () => {
                return request(server)
                    .put("/dishes/1100001/vote")
                    .expect(404)
                    .expect({ message: "Invalid Dishes Id!" });
            });
        });
    })
});
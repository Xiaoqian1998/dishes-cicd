let mongoose = require('mongoose');

let DishesSchema = new mongoose.Schema({
        name: String,
        code: String,
        price: Number,
        upvotes: {type: Number, default: 0}
    },
    { collection: 'disheddb' });

module.exports = mongoose.model('Dishes', DishesSchema);
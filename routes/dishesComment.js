let DishesComment = require('../models/dishesComment');
let express = require('express');
let router = express.Router();
let mongoose = require('mongoose');

const mongodbUri = "mongodb://lxq:qmlpTrVYVmTqZ58s@cluster0-shard-00-00-szspm.mongodb.net:27017,cluster0-shard-00-01-szspm.mongodb.net:27017,cluster0-shard-00-02-szspm.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority";
mongoose.connect(mongodbUri);
let db = mongoose.connection;

db.on('error', function (err) {
    console.log('Unable to Connect to [ ' + db.name + ' ]', err);
});

db.once('open', function () {
    console.log('Successfully Connected to [ ' + db.name + ' ] on mlab.com');
});

router.findAll = (req, res) => {
    // Return a JSON representation of our list
    res.setHeader('Content-Type', 'application/json');
    DishesComment.find(function(err, comments) {
        if (err)
            res.send(err);
        res.send(JSON.stringify(comments,null,5));
    });
}

router.findOne = (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    DishesComment.find({ "_id" : req.params.id },function(err, dishesComment) {
        if (err)
            res.json({ message: 'DishesComment NOT Found!', errmsg : err } );
        else
            res.send(JSON.stringify(dishesComment,null,5));
    });
}

router.addComment = (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    var comment = new DishesComment();

    comment.title = req.body.title;
    comment.code = req.body.code;
    comment.message = req.body.message;

    if(req.body.id){
        DishesComment.findById(req.body.id, function(err,dishesComment) {
            if (err)
                res.json({ message: 'DishesComment NOT Found!', errmsg : err } );
            else {
                dishesComment.title = req.body.title;
                dishesComment.code = req.body.code;
                dishesComment.message = req.body.message;
                dishesComment.save(function (err) {
                    if (err)
                        res.json({ message: 'DishesComment NOT UpVoted!', errmsg : err } );
                    else
                        res.json({ message: 'DishesComment Successfully Upvoted!', data: dishesComment });
                });
            }
        });
        return
    }
    comment.save(function(err) {
        if (err)
            res.json({ message: 'DishesComment NOT Added!', errmsg : err } );
        else
            res.json({ message: 'DishesComment Successfully Added!', data: comment });
    });
}

router.deleteComment = (req, res) => {
    DishesComment.findByIdAndRemove(req.params.id, function(err) {
        if (err)
            res.json({ message: 'DishesComment NOT DELETED!', errmsg : err } );
        else
            res.json({ message: 'DishesComment Successfully Deleted!'});
    });
}

router.incrementUpvotes = (req, res) => {
    DishesComment.findById(req.params.id, function(err,dishesComment) {
        if (err)
            res.json({ message: 'DishedComment NOT Found!', errmsg : err } );
        else {
            dishesComment.upvotes += 1;
            dishesComment.save(function (err) {
                if (err)
                    res.json({ message: 'DishesComment NOT UpVoted!', errmsg : err } );
                else
                    res.json({ message: 'DishesComment Successfully Upvoted!', data: dishesComment });
            });
        }
    });
}

router.reductionUpvotes = (req, res) => {
    DishesComment.findById(req.params.id, function(err,dishesComment) {
        if (err)
            res.json({ message: 'DishedComment NOT Found!', errmsg : err } );
        else {
            if(dishesComment.upvotes < 1) return;
            dishesComment.upvotes -= 1;
            dishesComment.save(function (err) {
                if (err)
                    res.json({ message: 'DishesComment NOT UpVoted!', errmsg : err } );
                else
                    res.json({ message: 'DishesComment Successfully Upvoted!', data: dishesComment });
            });
        }
    });
}

module.exports = router;